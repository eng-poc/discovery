FROM openjdk:11-jdk
COPY target/*.jar discovery.jar
ENTRYPOINT ["java", "-jar", "/discovery.jar"]
EXPOSE 9001